# Activiti7 + SpringBoot2.1.0 示例

## 项目使用技术及相关框架
* maven 3.5.4
* Activiti 7
* SpringBoot 2.1.0.RELEASE 
* spring security
* thymeleaf
* spring-web
* druid
* mysql
* mybatis
* lombok
* guava

> 注意: 下载 Activiti 相关 jar 包的时候, 可能会出现因网络问题不能下载的问题, 
> 可以科学上网, 刷新 maven 前请先删除本地仓库中已经存储的内容

详细内容待续...