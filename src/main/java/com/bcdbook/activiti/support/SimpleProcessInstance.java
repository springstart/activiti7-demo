package com.bcdbook.activiti.support;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.activiti.bpmn.model.ActivitiListener;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.engine.impl.persistence.entity.*;

import java.util.Date;
import java.util.List;

/**
 * 自定义的流程实例对象
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-03-23 15:58
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class SimpleProcessInstance {

    private String id;
    private FlowElement currentFlowElement;
    private ActivitiListener currentActivitiListener;
    private ExecutionEntityImpl processInstance;
    private ExecutionEntityImpl parent;
    private List<ExecutionEntityImpl> executions;
    private ExecutionEntityImpl superExecution;
    private ExecutionEntityImpl subProcessInstance;
    private String tenantId = "";
    private String name;
    private String description;
    private String localizedName;
    private String localizedDescription;
    private Date lockTime;
    private boolean isActive = true;
    private boolean isScope = true;
    private boolean isConcurrent;
    private boolean isEnded;
    private boolean isEventScope;
    private boolean isMultiInstanceRoot;
    private boolean isCountEnabled;
    private String eventName;
    private List<EventSubscriptionEntity> eventSubscriptions;
    private List<JobEntity> jobs;
    private List<TimerJobEntity> timerJobs;
    private List<TaskEntity> tasks;
    private List<IdentityLinkEntity> identityLinks;
    private String deleteReason;
    private int suspensionState;
    private String startUserId;
    private Date startTime;
    private int eventSubscriptionCount;
    private int taskCount;
    private int jobCount;
    private int timerJobCount;
    private int suspendedJobCount;
    private int deadLetterJobCount;
    private int variableCount;
    private int identityLinkCount;
    private String processDefinitionId;
    private String processDefinitionKey;
    private String processDefinitionName;
    private Integer processDefinitionVersion;
    private String deploymentId;
    private String activityId;
    private String activityName;
    private String processInstanceId;
    private String businessKey;
    private String parentId;
    private String superExecutionId;
    private String rootProcessInstanceId;
    private ExecutionEntityImpl rootProcessInstance;
    private boolean forcedUpdate;
    private List<VariableInstanceEntity> queryVariables;
    private boolean isDeleted;
    private String parentProcessInstanceId;
}
