package com.bcdbook.activiti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 主启动类
 *
 * @author summer
 * @date 2019-01-04 18:34
 * @version V1.0.0-RELEASE
 */
@SpringBootApplication
public class ActivitiDemo2Application {

    public static void main(String[] args) {
        SpringApplication.run(ActivitiDemo2Application.class, args);
    }

}

