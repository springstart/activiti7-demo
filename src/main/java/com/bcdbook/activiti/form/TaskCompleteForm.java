package com.bcdbook.activiti.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import java.util.Map;

/**
 * 任务节点审批的表单
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-03-27 13:34
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Validated
public class TaskCompleteForm {

    /**
     * 任务节点 id
     */
    @NotBlank(message = "任务节点 id 不能为空")
    private String taskId;
    /**
     * 下一个审批人
     */
    private String assignee;
//    /**
//     * 候选人集合
//     */
//    private List<String> candidateUserList;
//    /**
//     * 候选人组集合
//     */
//    private List<String> candidateUserGroupList;
    /**
     * 表单的 key
     */
    private String formKey;
    /**
     * 自定义的参数
     */
    private Map<String, Object> taskValue;

}
