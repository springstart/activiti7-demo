package com.bcdbook.activiti.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import java.util.Map;

/**
 * 流程开启的表单对象
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-03-27 12:43
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Validated
public class ProcessStartForm {

    /**
     * 流程的 key
     */
    @NotBlank(message = "流程的 key 不能为空")
    private String processKey;
    /**
     * 业务的 key
     */
    @NotBlank(message = "业务的 key 不能为空")
    private String businessKey;
    /**
     * 自定义的流程的名称
     */
    @NotBlank(message = "流程的自定义名称不能为空")
    private String processName;
    /**
     * 自定义的参数
     */
    private Map<String, Object> processValue;
}
