package com.bcdbook.activiti.listener;

import lombok.extern.slf4j.Slf4j;
import org.activiti.api.task.runtime.events.TaskAssignedEvent;
import org.activiti.api.task.runtime.events.TaskCompletedEvent;
import org.activiti.api.task.runtime.events.listener.TaskRuntimeEventListener;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * 任务监听器
 *
 * @author summer
 * @date 2018-12-29 22:50
 * @version V1.0.0-RELEASE
 */
@Component
@Slf4j
public class TaskListener {

    /**
     * 任务认领的监听器
     *
     * @author summer
     * @date 2018-12-29 18:33
     * @return org.activiti.api.task.runtime.events.listener.TaskRuntimeEventListener<org.activiti.api.task.runtime.events.TaskAssignedEvent>
     * @version V1.0.0-RELEASE
     */
    @Bean
    public TaskRuntimeEventListener<TaskAssignedEvent> taskAssignedListener() {
        return taskAssigned -> log.info(">>> 任务: '{}' 被认领了, 认领者是: '{}'",
                taskAssigned.getEntity().getName(),
                taskAssigned.getEntity().getAssignee());
    }

    /**
     * 任务处理的监听器
     *
     * @author summer
     * @date 2018-12-29 18:33
     * @return org.activiti.api.task.runtime.events.listener.TaskRuntimeEventListener<org.activiti.api.task.runtime.events.TaskCompletedEvent>
     * @version V1.0.0-RELEASE
     */
    @Bean
    public TaskRuntimeEventListener<TaskCompletedEvent> taskCompletedListener() {
        return taskCompleted -> log.info(">>> 任务: '{}' 被处理了, 处理者是: '{}', 你可以发个通知给对应的用户.",
                taskCompleted.getEntity().getName(),
                taskCompleted.getEntity().getOwner());
    }
}
