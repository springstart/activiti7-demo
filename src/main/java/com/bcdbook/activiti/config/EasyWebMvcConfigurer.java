//package com.bcdbook.activiti.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.cors.CorsConfiguration;
//import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
//import org.springframework.web.filter.CorsFilter;
//import org.springframework.web.servlet.config.annotation.CorsRegistry;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
///**
// * web 请求的配置类
// *
// * @author summer
// * @version V1.0.0-RELEASE
// * DateTime 2019-03-27 18:28
// */
//@Configuration
//@EnableWebMvc
//public class EasyWebMvcConfigurer implements WebMvcConfigurer {
//
//    /**
//     * 跨域请求的配置
//     *
//     * @param registry 跨域注册对象
//     * @return void
//     * Author summer
//     * Version V1.0.0-RELEASE
//     * DateTime 2019-03-27 18:29
//     */
//    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        //设置允许跨域的路径
//        registry.addMapping("/**")
//                //设置允许跨域请求的域名
//                .allowedOrigins("*")
//                //是否允许证书 不再默认开启
//                .allowCredentials(true)
//                //设置允许的方法
//                .allowedMethods("*")
//                //跨域允许时间
//                .maxAge(3600);
//    }
//
//    /**
//     * 跨域拦截器的配置
//     *
//     * @return org.springframework.web.filter.CorsFilter
//     * Author summer
//     * Version V1.0.0-RELEASE
//     * DateTime 2019-03-27 20:09
//     */
//    @Bean
//    public CorsFilter corsFilter() {
//        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//        source.registerCorsConfiguration("/**", buildConfig());
//        return new CorsFilter(source);
//    }
//
//    /**
//     * 封装跨域拦截器的配置
//     *
//     * @return org.springframework.web.cors.CorsConfiguration
//     * Author summer
//     * Version V1.0.0-RELEASE
//     * DateTime 2019-03-27 20:09
//     */
//    private CorsConfiguration buildConfig() {
//        CorsConfiguration corsConfiguration = new CorsConfiguration();
//        corsConfiguration.addAllowedOrigin("*");
//        corsConfiguration.addAllowedHeader("*");
//        corsConfiguration.addAllowedMethod("*");
//        corsConfiguration.addExposedHeader("Authorization");
//        return corsConfiguration;
//    }
//
//    /**
//     * 静态资源配置
//     *
//     * @param registry 资源处理程序注册表
//     * @return void
//     * Author summer
//     * Version V1.0.0-RELEASE
//     * DateTime 2019-03-27 18:30
//     */
//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        // 处理静态文件的映射
//        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
//        // 处理 swagger 的映射问题
//        registry.addResourceHandler("swagger-ui.html")
//                .addResourceLocations("classpath:/META-INF/resources/");
//        registry.addResourceHandler("/webjars/**")
//                .addResourceLocations("classpath:/META-INF/resources/webjars/");
//    }
//
////    /**
////     * 加入 put 请求的过滤器
////     * 若不添加此过滤器, put 请求则无法传值
////     *
////     * @author summer
////     * @date 2018/10/30 4:23 PM
////     * @return org.springframework.web.filter.HttpPutFormContentFilter
////     * @version V1.0.0-RELEASE
////     */
////    @Bean
////    public HttpPutFormContentFilter httpPutFormContentFilter() {
////        return new HttpPutFormContentFilter();
////    }
//}
