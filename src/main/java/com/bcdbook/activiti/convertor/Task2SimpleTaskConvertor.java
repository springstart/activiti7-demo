package com.bcdbook.activiti.convertor;
import com.bcdbook.activiti.support.SimpleTask;
import org.activiti.engine.task.Task;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 任务转换成简单任务对象的转换器
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-03-22 15:07
 */
public class Task2SimpleTaskConvertor {

    /**
     * 对 EngineTask 的集合做转换
     *
     * @param taskList EngineTask 集合
     * @return java.util.List<com.bcdbook.activiti.support.SimpleTask>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-03-22 15:12
     */
    public static List<SimpleTask> convert(List<Task> taskList){
        // 参数校验
        if (CollectionUtils.isEmpty(taskList)) {
            return null;
        }

        // 创建返回对象
        return taskList.stream()
                .filter(Objects::nonNull)
                .map(Task2SimpleTaskConvertor::convert)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    /**
     * engine Taks 转换成自定义的 Task 的转换器
     *
     * @param task EngineTask
     * @return com.bcdbook.activiti.support.SimpleTask
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-03-22 15:10
     */
    public static SimpleTask convert(Task task){
        // 参数校验
        if (task == null) {
            return null;
        }
        // 定义转换成的对象
        SimpleTask simpleTask = new SimpleTask();
        simpleTask.setId(task.getId());
        simpleTask.setOwner(task.getOwner());
//        simpleTask.setAssigneeUpdatedCount(task.);
//        simpleTask.setOriginalAssignee(task.get);
        simpleTask.setAssignee(task.getAssignee());
        simpleTask.setDelegationState(task.getDelegationState());
        simpleTask.setParentTaskId(task.getParentTaskId());
        simpleTask.setName(task.getName());
//        simpleTask.setLocalizedName(task.geL);
        simpleTask.setDescription(task.getDescription());
//        simpleTask.setLocalizedDescription(task.ge);
        simpleTask.setPriority(task.getPriority());
        simpleTask.setCreateTime(task.getCreateTime());
        simpleTask.setDueDate(task.getDueDate());
//        simpleTask.setSuspensionState(task.geS);
        simpleTask.setCategory(task.getCategory());
//        simpleTask.setIdentityLinksInitialized(task.getI);
//        simpleTask.setTaskIdentityLinkEntities(task.getT);
        simpleTask.setExecutionId(task.getExecutionId());
//        simpleTask.setExecution(task.getE);
        simpleTask.setProcessInstanceId(task.getProcessInstanceId());
//        simpleTask.setProcessInstance(task.ge);
        simpleTask.setProcessDefinitionId(task.getProcessDefinitionId());
        simpleTask.setTaskDefinitionKey(task.getTaskDefinitionKey());
        simpleTask.setFormKey(task.getFormKey());
//        simpleTask.setDeleted(task.getDe);
//        simpleTask.setCanceled(task.getCa);
//        simpleTask.setEventName(task.geE);
//        simpleTask.setCurrentActivitiListener(task.geC);
        simpleTask.setTenantId(task.getTenantId());
//        simpleTask.setQueryVariables(task.getQ);
//        simpleTask.setForcedUpdate(task.getF);
        simpleTask.setClaimTime(task.getClaimTime());

        // 返回转换后的对象
        return simpleTask;
    }

}
