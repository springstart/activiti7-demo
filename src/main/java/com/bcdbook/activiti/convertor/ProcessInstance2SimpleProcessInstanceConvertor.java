package com.bcdbook.activiti.convertor;
import com.bcdbook.activiti.support.SimpleProcessInstance;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 流程实例转换成简单流程实例对象的转换器
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-03-23 16:00
 */
public class ProcessInstance2SimpleProcessInstanceConvertor {

    /**
     * 转换集合
     *
     * @param processInstanceList 流程实例集合
     * @return java.util.List<com.bcdbook.activiti.support.SimpleProcessInstance>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-03-23 16:11
     */
    public static List<SimpleProcessInstance> convert(List<ProcessInstance> processInstanceList) {
        // 参数校验
        if (CollectionUtils.isEmpty(processInstanceList)) {
            return null;
        }

        // 集合转换并返回转换后的对象
        return processInstanceList.stream()
                .map(ProcessInstance2SimpleProcessInstanceConvertor::convert)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    /**
     * 单个对象的转换
     *
     * @param processInstance 流程实例对象
     * @return com.bcdbook.activiti.support.SimpleProcessInstance
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-03-23 16:01
     */
    public static SimpleProcessInstance convert(ProcessInstance processInstance) {
        // 参数校验
        if (processInstance == null) {
            return null;
        }
        // 创建简单执行对象
        SimpleProcessInstance simpleprocessinstance = new SimpleProcessInstance();
        simpleprocessinstance.setId(processInstance.getId());
//        simpleprocessinstance.setCurrentFlowElement(processInstance.cu);
//        simpleprocessinstance.setCurrentActivitiListener(processInstance);
//        simpleprocessinstance.setProcessInstance(processInstance.proI);
//        simpleprocessinstance.setParent(processInstance.gePa);
//        simpleprocessinstance.setExecutions(processInstance.getE);
//        simpleprocessinstance.setSuperExecution(processInstance.);
//        simpleprocessinstance.setSubProcessInstance(processInstance.getS);
        simpleprocessinstance.setTenantId(processInstance.getTenantId());
        simpleprocessinstance.setName(processInstance.getName());
        simpleprocessinstance.setDescription(processInstance.getDescription());
        simpleprocessinstance.setLocalizedName(processInstance.getLocalizedName());
        simpleprocessinstance.setLocalizedDescription(processInstance.getLocalizedDescription());
//        simpleprocessinstance.setLockTime(processInstance.getL);
//        simpleprocessinstance.setActive(processInstance.getA);
//        simpleprocessinstance.setScope(processInstance.getS);
//        simpleprocessinstance.setConcurrent(processInstance.getC);
//        simpleprocessinstance.setEnded(processInstance.getE);
//        simpleprocessinstance.setEventScope(processInstance.getE);
//        simpleprocessinstance.setMultiInstanceRoot(processInstance.getM);
//        simpleprocessinstance.setCountEnabled(processInstance.getC);
//        simpleprocessinstance.setEventName(processInstance.getE);
//        simpleprocessinstance.setEventSubscriptions(processInstance);
//        simpleprocessinstance.setJobs(processInstance.getJ);
//        simpleprocessinstance.setTimerJobs(processInstance.getT);
//        simpleprocessinstance.setTasks(processInstance.getTa);
//        simpleprocessinstance.setIdentityLinks(processInstance.getI);
//        simpleprocessinstance.setDeleteReason(processInstance.getDe);
//        simpleprocessinstance.setSuspensionState(processInstance.getSu);
        simpleprocessinstance.setStartUserId(processInstance.getStartUserId());
        simpleprocessinstance.setStartTime(processInstance.getStartTime());
//        simpleprocessinstance.setEventSubscriptionCount(processInstance.getE);
//        simpleprocessinstance.setTaskCount(processInstance.tas);
//        simpleprocessinstance.setJobCount(processInstance.getJ);
//        simpleprocessinstance.setTimerJobCount(processInstance.getT);
//        simpleprocessinstance.setSuspendedJobCount(processInstance.getS);
//        simpleprocessinstance.setDeadLetterJobCount(processInstance.getD);
//        simpleprocessinstance.setVariableCount(processInstance.getV);
//        simpleprocessinstance.setIdentityLinkCount(processInstance.getIde);
        simpleprocessinstance.setProcessDefinitionId(processInstance.getProcessDefinitionId());
        simpleprocessinstance.setProcessDefinitionKey(processInstance.getProcessDefinitionKey());
        simpleprocessinstance.setProcessDefinitionName(processInstance.getProcessDefinitionName());
        simpleprocessinstance.setProcessDefinitionVersion(processInstance.getProcessDefinitionVersion());
        simpleprocessinstance.setDeploymentId(processInstance.getDeploymentId());
        simpleprocessinstance.setActivityId(processInstance.getActivityId());
//        simpleprocessinstance.setActivityName(processInstance.getA);
        simpleprocessinstance.setProcessInstanceId(processInstance.getProcessInstanceId());
        simpleprocessinstance.setBusinessKey(processInstance.getBusinessKey());
        simpleprocessinstance.setParentId(processInstance.getParentId());
        simpleprocessinstance.setSuperExecutionId(processInstance.getSuperExecutionId());
        simpleprocessinstance.setRootProcessInstanceId(processInstance.getRootProcessInstanceId());
//        simpleprocessinstance.setRootProcessInstance(processInstance.getPro);
//        simpleprocessinstance.setForcedUpdate(processInstance.get);
//        simpleprocessinstance.setQueryVariables(processInstance.getQ);
//        simpleprocessinstance.setDeleted(processInstance.getDe);
        simpleprocessinstance.setParentProcessInstanceId(processInstance.getParentProcessInstanceId());

        return simpleprocessinstance;
    }
}
