package com.bcdbook.activiti.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * 浏览器环境下安全配置主类
 * WebSecurityConfigurerAdapter 是 web 应用下的做安全配置的适配器
 *
 * @author summer
 * @date 2018-12-20 17:48
 * @version V1.0.0-RELEASE
 */
@Configuration
public class BrowserSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * 配置认证方式为表单登录
     *
     * @author summer
     * @date 2018-12-20 17:51
     * @param http 安全请求
     * @return void
     * @version V1.0.0-RELEASE
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        // 处理生成图片时无法传递的问题
        http.headers().frameOptions().disable();
        /*
         * 设置成表单登录
         */
        // 指定身份认证的方式
        http.formLogin()
        // 使用 Basic 的方式进行登录
//        http.httpBasic()
                // 指定自己的登录页面
                //.loginPage("/loginPage")
                // 定义处理请求的接口, 告诉 Spring 此接口是用来处理登录请求的
                //.loginProcessingUrl("/loginForm")
                .and()
                // 授权的配置
                .authorizeRequests()
                /*
                 * 注意: 此处需要配置忽略的路径, 否则自定义的登录页面是无法跳转的
                 */
                //.requestMatchers(CorsUtils::isPreFlightRequest).permitAll()
                .antMatchers("/static/**").permitAll()
                // 任何请求
                .anyRequest()
                // 都需要身份认证
                .authenticated()
                .and()
                // 关闭 csrf 防伪造攻击, 新版本中, 若不关闭此接口, 不会抛出异常
                // 正式上线的产品最好不要关闭此功能
                .csrf().disable();

//        super.configure(http);
    }
}
