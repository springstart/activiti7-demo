package com.bcdbook.activiti.web.history;

import org.activiti.engine.HistoryService;
import org.activiti.engine.history.HistoricDetail;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.history.HistoricVariableInstance;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 历史任务的控制器
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-03-22 13:40
 */
@RestController
@RequestMapping("/task/history")
public class TaskHistoryController {

    /**
     * 注入历史 service
     */
    @Resource
    private HistoryService historyService;

    /**
     * 获取所有历史任务实例
     *
     * @return java.util.List<org.activiti.engine.history.HistoricTaskInstance>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-03-22 13:43
     */
    @GetMapping
    public List<HistoricTaskInstance> listAllHistoryTask(){

        return historyService.createHistoricTaskInstanceQuery()
                .list();
    }

    /**
     * 根据任务 id 获取历史任务
     *
     * @param taskId 任务 id
     * @return org.activiti.engine.history.HistoricTaskInstance
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-03-27 14:42
     */
    @GetMapping("/{taskId}")
    public HistoricTaskInstance getByTaskId(@PathVariable(name = "taskId") String taskId){
        // 执行查询并返回结果
        HistoricTaskInstance historicTaskInstance = historyService.createHistoricTaskInstanceQuery()
                .taskId(taskId)
                .singleResult();
        // 判断当前历史任务是否为空, 如果为空, 则直接返回空
        if (historicTaskInstance == null) {
            return null;
        }

        List<HistoricVariableInstance> historicVariableInstanceList = historyService
                .createHistoricVariableInstanceQuery()
                .taskId(taskId)
                .list();

        // 查询当前任务的历史详情
        List<HistoricDetail> taskHistoricDetailList = historyService
                .createHistoricDetailQuery()
                .taskId(historicTaskInstance.getId())
                .list();

        return historicTaskInstance;
    }

    /**
     * 根据流程实例 id 查询历史任务节点
     *
     * @param processInstanceId 流程实例 id
     * @return java.util.List<org.activiti.engine.history.HistoricTaskInstance>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-03-27 13:27
     */
    @GetMapping("/process/{processInstanceId}")
    public List<HistoricTaskInstance> listHistoryTaskBy(@PathVariable(name = "processInstanceId")
                                                                    String processInstanceId){
        return historyService.createHistoricTaskInstanceQuery()
                .processInstanceId(processInstanceId)
                .list();
    }

    /**
     * 根据任务所有者, 获取历史任务的实例
     *
     * @param owner 任务所有者
     * @return java.util.List<org.activiti.engine.history.HistoricTaskInstance>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-03-22 13:43
     */
    @GetMapping("/owner")
    public List<HistoricTaskInstance> listHistoryTaskByOwner(String owner){

        return historyService.createHistoricTaskInstanceQuery()
                .taskOwner(owner).list();
    }
}
