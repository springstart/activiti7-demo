package com.bcdbook.activiti.web.history;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.history.HistoricProcessInstance;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 历史流程的控制器
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-03-22 13:40
 */
@RestController
@RequestMapping("/process/history")
public class ProcessHistoryController {

    /**
     * 注入流程引擎
     */
    @Resource
    private ProcessEngine processEngine;

    /**
     * 根据业务的 key 获取流程历史对象
     *
     * @param businessKey 业务的 key
     * @return java.util.List<org.activiti.engine.history.HistoricProcessInstance>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-03-22 17:09
     */
    @GetMapping("/business")
    public List<HistoricProcessInstance> listByBusinessKey(String businessKey){
        return processEngine.getHistoryService()
                .createHistoricProcessInstanceQuery()
                .processInstanceBusinessKey(businessKey)
                .list();
    }

    /**
     * 根据开启用户的 id 获取流程对象
     *
     * @param userId 用户 id
     * @return java.util.List<org.activiti.engine.history.HistoricProcessInstance>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-03-22 17:25
     */
    @GetMapping("/user")
    public List<HistoricProcessInstance> listByStartUserId(String userId){
        return processEngine.getHistoryService()
                .createHistoricProcessInstanceQuery()
                .startedBy(userId)
                .list();
    }
}
