package com.bcdbook.activiti.web;

import com.bcdbook.activiti.security.SecurityUtil;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * 基础内容的 controller
 *
 * @author summer
 * @date 2019-01-04 19:04
 * @version V1.0.0-RELEASE
 */
@Controller
public class CommonController {

    /**
     * 注入 security 的工具类
     */
    @Resource
    private SecurityUtil securityUtil;

    /**
     * 获取当前在线用户
     *
     * @author summer
     * @date 2019-01-04 15:13
     * @return java.lang.String
     * @version V1.0.0-RELEASE
     */
    @RequestMapping("/user")
    @ResponseBody
    public UserDetails getOnlineUser(){
        // 获取当前登录用户
        return securityUtil.getActiveUserDetails();
    }

    @GetMapping
    public String index(){
        return "index";
    }
}
